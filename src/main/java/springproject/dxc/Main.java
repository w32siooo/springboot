package springproject.dxc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

@SpringBootApplication
public class Main {
	// Setup

	public static void main(String[] args) throws SQLException {

		SpringApplication.run(Main.class, args);
	}

}
