package springproject.dxc.data_access.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import springproject.dxc.data_access.utils.ConnectionHelper;
import springproject.dxc.data_access.interfaces.TrackRepository;
import springproject.dxc.models.Track;
import java.sql.*;
import java.util.LinkedList;

@Repository
public class TrackRepositoryImpl implements TrackRepository {

    // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;

    Logger logger = LoggerFactory.getLogger(TrackRepositoryImpl.class);

    @Override
    public LinkedList<Track> get5Tracks() {
        LinkedList<Track> trackLinkedList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT TrackId, T.Name AS TrackTitle, Composer AS Artist, A.Title AS Album, G.Name AS Genre\n" +
                            "FROM Track T\n" +
                            "INNER JOIN Album A on A.AlbumId = T.AlbumId\n" +
                            "INNER JOIN Genre G on G.GenreId = T.GenreId\n" +
                            "ORDER BY RANDOM()\n" +
                            "LIMIT 5");

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                trackLinkedList.add(new Track(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("TrackTitle"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                ));
            }
            logger.info("Run get5Tracks successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return trackLinkedList;
    }

    @Override
    public LinkedList<Track> searchTracksByName(String title) {
        LinkedList<Track> trackLinkedList = new LinkedList<>();

        try {
            //Connect to the database with JDBC.
            conn = DriverManager.getConnection(URL);
            logger.info("Connection established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT TrackId, T.Name AS TrackTitle, Composer AS Artist, A.Title AS Album, G.Name AS Genre\n" +
                            "FROM Track T\n" +
                            "INNER JOIN Album A on A.AlbumId = T.AlbumId\n" +
                            "INNER JOIN Genre G on G.GenreId = T.GenreId\n" +
                            "WHERE TrackTitle LIKE ?");

            String preparedString = "%" + title + "%";
            preparedStatement.setString(1, preparedString);

            // Execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                trackLinkedList.add(new Track(
                        resultSet.getInt("TrackId"),
                        resultSet.getString("TrackTitle"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                ));
            }
            logger.info("Run searchTracksByName successfully.");

        } catch (SQLException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                conn.close();
                logger.info("Connection closed successfully.");
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }

        return trackLinkedList;
    }
}
