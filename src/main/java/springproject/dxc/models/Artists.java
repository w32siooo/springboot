package springproject.dxc.models;

public class Artists {
    private String artistName;

    public Artists(String artistName) {
        this.artistName = artistName;
    }

    public String getArtistName() {
        return artistName;
    }

    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }
}
